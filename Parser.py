from enum import Enum
from Tokens import *

class ParsingException(Exception):
    pass

class NodeType:
    NUMBER = 0
    BOOLEAN = 1
    VARIABLE = 2
    PRODUCT = 3
    DIVISION = 4
    SUM = 5
    DIFFERENCE = 6
    GEQ = 7
    LEQ = 8
    GT = 9
    LT = 10
    COMP = 11
    NCOMP = 12
    NOT = 13
    FCALL = 14
    FDECL = 15
    IF = 16
    LOOP = 17
    OR = 18
    AND = 19
    GROUP = 20
    FNAME = 21
    DECL = 22
    ASSIGN = 23
    RETURN = 24
    NEG = 25
    MODULO = 26
    STRING = 27

def parse(toks):
    return parseProgram(toks)[0]

def parseProgram(toks):
    statements = []
    while toks != []:
        st,toks = parseStatement(toks)
        statements.append(st)
    return statements,toks

def parseStatement(toks):
    headType = toks[0].tType
    if headType == TokenType.RETURN:
        r,toks = parseValExpr(toks[1:])
        if toks[0].tType != TokenType.SEMICOLON:
            raise ParsingException("Missing ';' after '->'")
        return (NodeType.RETURN,r), toks[1:]
    elif headType == TokenType.LWHILE:
        return parseWhile(toks)
    elif headType == TokenType.LGROUP:
        return parseGroup(toks)
    elif headType == TokenType.IDENTIFIER:
        secondType = toks[1].tType
        if secondType == TokenType.EQUAL:
            return parseAssign(toks)
        elif secondType == TokenType.ASSIGN or secondType == TokenType.SEMICOLON:
            return parseDeclaration(toks)
        elif secondType == TokenType.LPAREN:
            head,toks = parseFunctionHead(toks)
            if toks[0].tType == TokenType.EQUIV:
                if not isDeclarationHead(head):
                    raise ParsingException("Function declaration must contain only identifiers as arguments in " + str(head))
                body,toks = parseStatement(toks[1:])
                return (NodeType.FDECL, head, body),toks
            elif toks[0].tType == TokenType.SEMICOLON:
                return (NodeType.FCALL, head),toks[1:]
            else:
                raise ParsingException("Function call must end with a ';' ")
        else:
            return parseCondition(toks)
    else:
        return parseCondition(toks)

def parseWhile(toks):
    if toks[0].tType != TokenType.LWHILE:
        raise ParsingException("Unexpected Token while parsing loop in " + strToks(toks) )
    cond,toks = parseBooleanExpression(toks[1:])
    if toks[0].tType != TokenType.RWHILE:
        raise ParsingException("Unexpected Token while parsing loop in " + strToks(toks) )
    st,toks = parseStatement(toks[1:])
    return (NodeType.LOOP, cond, st), toks

def parseGroup(toks):
    sts = []
    if toks[0].tType != TokenType.LGROUP:
        raise ParsingException("Unexpected Token while parsing group, got " + str(toks[0].tType))
    toks = toks[1:]
    while toks[0].tType != TokenType.RGROUP:
        st,toks = parseStatement(toks)
        sts.append(st)
    return (NodeType.GROUP, sts), toks[1:]

def parseAssign(toks):
    if toks[0].tType != TokenType.IDENTIFIER:
        raise ParsingException("Missing identifier in assignment")
    var = toks[0].val
    toks = toks[1:]
    if toks[0].tType != TokenType.EQUAL:
        raise ParsingException("Unexpected Token while parsing assignment")
    val,toks = parseValExpr(toks[1:])
    if toks[0].tType != TokenType.SEMICOLON:
        raise ParsingException("Missing semicolon after assignment")
    return (NodeType.ASSIGN, var, val), toks[1:]

def parseDeclaration(toks):
    if toks[0].tType != TokenType.IDENTIFIER:
        raise ParsingException("Missing identifier in declaration")
    var = toks[0].val
    toks = toks[1:]
    if toks[0].tType == TokenType.SEMICOLON:
        return (NodeType.DECL, var, (NodeType.NUMBER, None)),toks[1:]
    elif toks[0].tType == TokenType.ASSIGN:
        val,toks = parseValExpr(toks[1:])
        if toks[0].tType != TokenType.SEMICOLON:
            raise ParsingException("Missing semicolon after declaration")
        return (NodeType.DECL, var, val),toks[1:] 

def parseFunctionHead(toks):
    name = toks[0]
    l = toks[1]
    if name.tType != TokenType.IDENTIFIER:
        raise ParsingException("Function head missing identifier, found " + strToks([name]))
    if l.tType != TokenType.LPAREN:
        raise ParsingException("Unexpected token while parsing function head")
    toks = toks[1:]
    args = []
    toks = toks[1:]
    while toks[0].tType != TokenType.RPAREN:
        arg, toks = parseValExpr(toks)
        args.append(arg)
        if toks[0].tType != TokenType.RPAREN and toks[0].tType != TokenType.COMMA:
            raise ParsingException("Missing ',' to separate parameters")
        elif toks[0].tType == TokenType.COMMA:
            toks = toks[1:]

    return (NodeType.FNAME, name.val, args), toks[1:]

def parseFunctionCall(toks):
    l,toks = parseFunctionHead(toks)
    return (NodeType.FCALL, l),toks

def isDeclarationHead(head):
    args = head[2]
    return len([x for x in args if x[0] != NodeType.VARIABLE]) == 0

def parseCondition(toks):
    cond,toks = parseBooleanExpression(toks)
    if toks[0].tType != TokenType.IMPLIES:
        raise ParsingException("Unexpected token while parsing conditional")
    
    body, toks = parseStatement(toks[1:])
    otherBranch = None
    if toks != [] and toks[0].tType == TokenType.ELSE:
        otherBranch, toks = parseStatement(toks[1:])
    return (NodeType.IF, cond, body, otherBranch), toks

def parseBooleanExpression(toks):
    l,toks = parseAnd(toks)
    while toks[0].tType == TokenType.OR:
        l2, toks = parseAnd(toks[1:])
        l = (NodeType.OR, l, l2)
    return l, toks

def parseAnd(toks):
    l,toks = parseNot(toks)
    while toks[0].tType == TokenType.AND:
        l2, toks = parseNot(toks[1:])
        l = (NodeType.AND, l, l2)
    return l, toks

def parseNot(toks):
    if toks[0].tType != TokenType.NOT:
        return parseComp(toks)
    r,toks = parseComp(toks[1:])
    return (NodeType.NOT, r), toks

def parseComp(toks):
    lIsArithmetic = False
    l = None
    a = None
    b = None
    try:
        b = parseAtomicBoolean(toks)
        a = parseArithmeticExpression(toks)
        if len(a[1]) > len(b[1]):
            l,toks = b
        else:
            l,toks = a
            lIsArithmetic = True
    except ParsingException:
        if b == None:
            l,toks = parseArithmeticExpression(toks)
            lIsArithmetic = True
        else:
            l,toks = b

    if l[0] == NodeType.VARIABLE:
        lIsArithmetic = True
    if toks != []:
        if toks[0].tType == TokenType.EQUAL_EQUAL:
            r,toks = parseValExpr(toks[1:])
            return (NodeType.COMP, l, r), toks
        elif toks[0].tType == TokenType.DIFFERENT:
            r,toks = parseValExpr(toks[1:])
            return (NodeType.NCOMP, l, r), toks
        elif lIsArithmetic:
            if toks[0].tType == TokenType.LT:
                r,toks = parseArithmeticExpression(toks[1:])
                return (NodeType.LT, l, r), toks
            elif toks[0].tType == TokenType.GT:
                r,toks = parseArithmeticExpression(toks[1:])
                return (NodeType.GT, l, r), toks
            elif toks[0].tType == TokenType.LEQ:
                r,toks = parseArithmeticExpression(toks[1:])
                return (NodeType.LEQ, l, r), toks
            elif toks[0].tType == TokenType.GEQ:
                r,toks = parseArithmeticExpression(toks[1:])
                return (NodeType.GEQ, l, r), toks
    if  lIsArithmetic or l == None:
        raise ParsingException("Error while parsing boolean expression, found non boolean expression")
    return l,toks

def parseAtomicBoolean(toks):
    head = toks[0]
    if head.tType == TokenType.LPAREN:
        l,toks = parseBooleanExpression(toks[1:])
        if toks[0].tType != TokenType.RPAREN:
            raise ParsingException("Missing ')'")
        return l,toks[1:]
    elif head.tType == TokenType.BOOLEAN:
        return (NodeType.BOOLEAN, head.val), toks[1:]
    elif head.tType == TokenType.IDENTIFIER:
        try:
            return parseFunctionCall(toks)
        except ParsingException:
            return (NodeType.VARIABLE,head.val),toks[1:]
    raise ParsingException("Not an atomic boolean expression")

def parseValExpr(toks):
    b = None
    a = None
    try:
        b = parseBooleanExpression(toks)
        a = parseArithmeticExpression(toks)
        #Return the one matching the most tokens
        if len(b[1]) > len(a[1]):
                return a
        else:
            return b
    except ParsingException:
        if b != None:
            return b
    
    try:
        return parseArithmeticExpression(toks)
    except ParsingException:
        pass

    raise ParsingException("Error, tried parsing Value Group expression found neither arithmetic nor boolean expression in : " + strToks(toks))

def parseArithmeticExpression(toks):
    l,toks = parseProduct(toks)
    while toks[0].tType == TokenType.PLUS or toks[0].tType == TokenType.MINUS:
        nType = NodeType.SUM if toks[0].tType == TokenType.PLUS else NodeType.DIFFERENCE
        l2, toks = parseProduct(toks[1:])
        l = (nType, l, l2)
    return l, toks

def parseProduct(toks):
    if toks[0].tType == TokenType.STRING:
        return (NodeType.STRING, toks[0].val), toks[1:]
    l,toks = parseUnaryMinus(toks)
    while toks[0].tType == TokenType.STAR or toks[0].tType == TokenType.DIV or toks[0].tType == TokenType.MODULO:
        nType = NodeType.PRODUCT
        if toks[0].tType == TokenType.DIV:
            nType = NodeType.DIVISION
        elif toks[0].tType == TokenType.MODULO:
            nType = NodeType.MODULO
        l2,toks = parseUnaryMinus(toks[1:])
        l = (nType, l, l2)
    return l, toks

def parseUnaryMinus(toks):
    if toks[0].tType == TokenType.MINUS:
        r,toks = parseAtomicArithmetic(toks[1:])
        return (NodeType.NEG , r), toks
    return parseAtomicArithmetic(toks)

def parseAtomicArithmetic(toks):
    head = toks[0]
    if head.tType == TokenType.LPAREN:
        l,toks = parseArithmeticExpression(toks[1:])
        if toks[0].tType != TokenType.RPAREN:
            raise ParsingException("Missing ')'")
        return l,toks[1:]
    elif head.tType == TokenType.NUMBER:
        return (NodeType.NUMBER, head.val), toks[1:]
    elif head.tType == TokenType.IDENTIFIER:
        try:
            return parseFunctionCall(toks)
        except ParsingException:
            return (NodeType.VARIABLE, head.val), toks[1:]
    raise ParsingException("Not an atomic number expression " + strToks(toks))
