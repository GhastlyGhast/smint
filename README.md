# Smint : a toy language to learn grammar parsing and interpreting

## Intro

Smint is a small imperative language with an interpreter written in python. It was made for the purpose of learning how a lexer and a recursive descent parser are written and to try to figure out a way to interpret it once parsed.

## Running
To launch an interactive REPL do :
```
python Smint.py
```
To execute a file or multiple files do :
```
python Smint.py file1 file2 file3 ... fileN
```


## Syntax
The somewhat formal grammar of Smint can be found in the file Grammar.txt, but here's an overview of it :

### Variables
A variable's name must start with a lowercase letter or an underscore and be composed of letters, underscores and digits.

A variable is declared like so :
```
x;
```

Or initialized when declared like so :
```
x := 0;
```

You can assign a value to a variable using this syntax :

```
x = 0;
```

There are only two types of values in Smint, booleans and numbers, all numbers are floating point, the two boolean values are 'True' and 'False'.

### Arithmetic
Arithmetic is standard, the available operators are (in order of increasing precedence) : (+, - (binary) ), (*, /, %), - (unary).
Parenthesis can be used to group expressions and change precedence.

### Boolean logic
You can compare two values using the equality operator == and the difference operator /= .
The available logical operators are (in order of increasing precedence) : ||, &&, ! .
Parenthesis can be used to group expressions and change precedence.

### Conditionals
A conditional is formed by a boolean expression followed by a => and a statement to be executed if the condition is met and eventually a -| and an statement to be executed if the condition isn't, you can chain -| and => .

Example :
```
   x == 5 => print(5);
-| x == 6 => print(6);
-| print(0);
```

### Loops
A loop is formed by a boolean expression surrounded by a -[ and a ]- and followed by a statement to be executed while the condition is true.

Example :
```
k := 0;
-[ k < 30 ]-
>(
    k = k + 1;
    print(k);
)<
```

### Functions
You can declare a function by specifying its name (following the same convention as the variable names) followed by a list of arguments between parenthesis and separated by commas and then a <=> and its body which must be a statement.

Example :
```
f(n,x) <=> print(n);
```

You can return a value from a function using a -> .

Example :
```
f(n) <=> -> n * 3 + 1;
```

### Statement
A statement is either a variable or function declaration, a variable assignment, a function call, a return or a group of starements contained between a >( and a )< .

Example :
```
f(n) <=> >(
            x := n + 6;
            x = x + 5 * n / x + 1;
            -> x;
         )<
```
