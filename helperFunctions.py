def isAlpha(c):
    return c.isalpha() or c == '_'

def isAlphaNum(c):
    return isAlpha(c) or c.isdigit()

def isNumberType(x):
    return type(x) is int or type(x) is float
