from enum import Enum

class TokenType(Enum):
    AND = 0
    OR = 1
    NOT = 2
    IDENTIFIER = 3
    EQUAL = 4
    IMPLIES = 5
    EQUIV = 6
    LPAREN = 7
    RPAREN = 8
    BOOLEAN = 9
    NUMBER = 10
    DIFFERENT = 11
    EQUAL_EQUAL = 12
    LEQ = 13
    GEQ = 14
    GT = 15
    LT = 16
    PLUS = 17
    MINUS = 18
    STAR = 19
    DIV = 20
    COMMA = 21
    SEMICOLON = 22
    ELSE = 23
    LWHILE = 24
    RWHILE = 25
    LGROUP = 26
    RGROUP = 27
    ASSIGN = 28
    RETURN = 29
    MODULO = 30
    STRING = 31

class Token:
    def __init__(self, tType, val = None):
        self.tType = tType
        self.val = val

    def __str__(self):
        return "Token Type : " + str(self.tType) + ("" if self.val == None else " Value : " + str(self.val))

def strToks(toks):
    s = "["
    for t in toks :
        s += ", " + str(t)
    return s + "]"
