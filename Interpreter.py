from Parser import NodeType
from helperFunctions import isNumberType

class InterpreterException(Exception):
    pass

class Context:
    def __init__(self, parent = None):
        self.parent = parent
        self.varTable = {}
        self.funcTable = {}

    def declareVar(self, name, val):
        if not name in self.varTable.keys():
            self.varTable[name] = val
        else:
            raise InterpreterException("Variable '" + name + "' already declared in current context")

    def assignToVar(self, name, value):
        if name in self.varTable.keys():
            self.varTable[name] = value
        elif self.parent != None:
            self.parent.assignToVar(name, value)
        else:
            raise InterpreterException("Assignment to undeclared variable '" + name + "'")

    def getVar(self, name):
        if name in self.varTable.keys():
            return self.varTable[name]
        elif self.parent != None:
            return self.parent.getVar(name)
        else:
            raise InterpreterException("Trying to access value of undeclared variable '" + name  + "'")
    
    def declareFunc(self, name, args, body):
        if not name in self.funcTable.keys():
            self.funcTable[name] = (args, body)
        else:
            raise InterpreterException("Function '" + name + "' already declared in current context")

    def getFunction(self, name):
        if name in self.funcTable.keys():
            return self.funcTable[name]
        elif self.parent != None:
            return self.parent.getFunction(name)
        else:
            raise InterpreterException("Call to undeclared function '" + name + "'")

    def runFunction(self, name, args):
        if name == "print":
            for a in args:
                print(interpret(a, self))
            return None
        elif name == "readNumber":
            if len(args) > 0:
                raise InterpreterException("Argument count mismatch while calling '" + name + "', expected 0 got " + str(len(args)))
            while True:
                try:
                    return float(input())
                except Exception:
                    pass
        elif name == "read":
            if len(args) > 0:
                raise InterpreterException("Argument count mismatch while calling '" + name + "', expected 0 got " + str(len(args)))
            return input()
        elif name == "repr":
            if len(args) != 1:
                raise InterpreterException("Argument count mismatch while calling '" + name + "', expected 1 got " + str(len(args)))
            return str(interpret(args[0], self))
        else:
            nContext = Context(parent = self)
            func = self.getFunction(name)
            argList = func[0]
            body = func[1]
            if len(argList) != len(args):
                raise InterpreterException("Argument count mismatch while calling '" + name + "', expected " + str(len(argList)) + " got " + str(len(args)))
            for (n,v) in zip(argList,args):
                nContext.declareVar(n[1],interpret(v,self))
            return interpret(body, nContext)
        

def interpret(tree,context):
    if tree[0] == NodeType.AND:
        return interpret(tree[1],context) and interpret(tree[2],context)
    elif tree[0] == NodeType.OR:
        return interpret(tree[1],context) or interpret(tree[2],context)
    elif tree[0] == NodeType.NOT:
        return not interpret(tree[1],context)
    elif tree[0] == NodeType.SUM:
        a = interpret(tree[1],context)
        b = interpret(tree[2],context)
        if isNumberType(a) and isNumberType(b) or isinstance(a, str) and isinstance(b, str):
            return a + b
        raise Exception("The + operator can only be used to add numbers or concatenate strings")
    elif tree[0] == NodeType.DIFFERENCE:
        a = interpret(tree[1],context)
        b = interpret(tree[2],context)
        if isNumberType(a) and isNumberType(b):
            return a - b
        raise InterpreterException("Error : You can only substract two numbers")
    elif tree[0] == NodeType.PRODUCT:
        a = interpret(tree[1],context)
        b = interpret(tree[2],context)
        if isNumberType(a) and isNumberType(b):
            return a * b
        raise InterpreterException("Error : You can only multiply two numbers")
    elif tree[0] == NodeType.DIVISION:
        a = interpret(tree[1],context)
        b = interpret(tree[2],context)
        if isNumberType(a) and isNumberType(b):
            return a / b
        raise InterpreterException("Error : You can only divide two numbers")
    elif tree[0] == NodeType.MODULO:
        a = interpret(tree[1],context)
        b = interpret(tree[2],context)
        if isNumberType(a) and isNumberType(b):
            return int(a) %  int(b)
        raise InterpreterException("Error : You can only take the rest of a division between two numbers")
    elif tree[0] == NodeType.GEQ:
        return interpret(tree[1],context) >= interpret(tree[2],context)
    elif tree[0] == NodeType.LEQ:
        return interpret(tree[1],context) <= interpret(tree[2],context)
    elif tree[0] == NodeType.GT:
        return interpret(tree[1],context) > interpret(tree[2],context)
    elif tree[0] == NodeType.LT:
        return interpret(tree[1],context) < interpret(tree[2],context)
    elif tree[0] == NodeType.COMP:
        return interpret(tree[1],context) == interpret(tree[2],context)
    elif tree[0] == NodeType.NCOMP:
        return interpret(tree[1],context) != interpret(tree[2],context)
    elif tree[0] == NodeType.NUMBER:
        return tree[1]
    elif tree[0] == NodeType.STRING:
        return tree[1]
    elif tree[0] == NodeType.NEG:
        return - interpret(tree[1], context)
    elif tree[0] == NodeType.BOOLEAN:
        return tree[1]
    elif tree[0] == NodeType.VARIABLE:
        return context.getVar(tree[1])
    elif tree[0] == NodeType.DECL:
        context.declareVar(tree[1], interpret(tree[2], context))
    elif tree[0] == NodeType.ASSIGN:
        context.assignToVar(tree[1], interpret(tree[2], context))
    elif tree[0] == NodeType.FCALL:
        return context.runFunction(tree[1][1], tree[1][2])
    elif tree[0] == NodeType.FDECL:
        context.declareFunc(tree[1][1], tree[1][2], tree[2])
    elif tree[0] == NodeType.IF:
        if interpret(tree[1], context):
            return interpret(tree[2], context)
        elif tree[3] != None:
            return interpret(tree[3], context)
    elif tree[0] == NodeType.LOOP:
        while interpret(tree[1], context):
            r = interpret(tree[2], context)
            if r != None:
                return r
    elif tree[0] == NodeType.GROUP:
        nContext = Context(parent = context)
        for s in tree[1]:
            r = interpret(s, nContext)
            if r != None:
                return r
    elif tree[0] == NodeType.RETURN:
       return interpret(tree[1], context) 

def runProgram(sts, context = None):
    if context == None:
        context = Context()
    for s in sts:
        interpret(s, context)
