from Tokens import *
from helperFunctions import * 

class Lexer:
    def __init__(self, stream):
        self.stream = stream
        self.start = 0
        self.current = 0
        self.tokens = []

    def addToken(self, tType, val = None):
        self.tokens.append(Token(tType, val))

    def isAtEnd(self):
        return self.current >= len(self.stream)

    def consume(self):
        if self.isAtEnd():
            return '\0'
        else:
            self.current += 1
            return self.stream[self.current - 1]

    def peek(self):
        if self.isAtEnd():
            return '\0'
        else:
            return self.stream[self.current]

    def boolean(self):
        if self.stream[self.start:self.start + 4] == "True":
            self.current = self.start + 4
            self.addToken(TokenType.BOOLEAN, True)
        elif self.stream[self.start:self.start + 5] == "False":
            self.current = self.start + 5
            self.addToken(TokenType.BOOLEAN, False)
        else:
            raise Exception("Unexpected character while parsing boolean")

    def number(self, pointFound = False):
        while self.peek().isdigit() or self.peek() == '.':
            if self.peek() == '.':
                if pointFound:
                    break
                else:
                    pointFound = True
            self.consume()
        s = self.stream[self.start:self.current]
        self.addToken(TokenType.NUMBER, float(s) if '.' in s else int(s))

    def identifier(self):
        while isAlphaNum(self.peek()):
            self.consume()
        self.addToken(TokenType.IDENTIFIER, self.stream[self.start:self.current])

    def tokenize(self):
        self.current = 0
        self.start = 0
        while not self.isAtEnd():
            self.start = self.current
            c = self.consume()
            if c == ' ' or c == '\r' or c == '\n' or c == '\t':
                pass
            elif c == '\"':
                s = ""
                c = self.consume()
                while c != '\"':
                    if self.isAtEnd():
                        raise Exception("Reached end of file while parsing string")
                    s += c
                    c = self.consume()
                self.addToken(TokenType.STRING, val = s)
            elif c == '#':
                while self.peek() != '\n' and self.peek() != '\0':
                    self.consume()
            elif c == '!':
                self.addToken(TokenType.NOT)
            elif c == '+':
                self.addToken(TokenType.PLUS)
            elif c == '-':
                if self.peek() == '|':
                    self.addToken(TokenType.ELSE)
                    self.consume()
                elif self.peek() == '[':
                    self.addToken(TokenType.LWHILE)
                    self.consume()
                elif self.peek() == '>':
                    self.addToken(TokenType.RETURN)
                    self.consume()
                else:
                    self.addToken(TokenType.MINUS)
            elif c == '&':
                if self.consume() == '&':
                    self.addToken(TokenType.AND)
                else:
                    raise Exception("Unexpected character, expected '&' after '&', got " + (" nothing" if self.isAtEnd() else self.peek()))
            elif c == '|':
                if self.consume() == '|':
                    self.addToken(TokenType.OR)
                else:
                    raise Exception("Unexpected character, expected '|' after '|', got " + (" nothing" if self.isAtEnd() else self.peek()))
            elif c == ']':
                if self.peek() == '-':
                    self.addToken(TokenType.RWHILE)
                    self.consume()
            elif c == '*':
                self.addToken(TokenType.STAR)
            elif c == '%':
                self.addToken(TokenType.MODULO)
            elif c == ',':
                self.addToken(TokenType.COMMA)
            elif c == ';':
                self.addToken(TokenType.SEMICOLON)
            elif c == '(':
                self.addToken(TokenType.LPAREN)
            elif c == ')':
                if self.peek() == '<':
                    self.addToken(TokenType.RGROUP)
                    self.consume()
                else:
                    self.addToken(TokenType.RPAREN)
            elif c == '/':
                if self.peek() == '=':
                    self.consume()
                    self.addToken(TokenType.DIFFERENT)
                else:
                    self.addToken(TokenType.DIV)
            elif c == ':':
                if self.peek() == '=':
                    self.consume()
                    self.addToken(TokenType.ASSIGN)
                else:
                    raise Exception("Unexpected character, expected '=' after ':', got " + (" nothing" if self.isAtEnd() else self.peek()))
            elif c == '=':
                if self.peek() == '<':
                    self.addToken(TokenType.LEQ)
                    self.consume()
                elif self.peek() == '=':
                    self.addToken(TokenType.EQUAL_EQUAL)
                    self.consume()
                elif self.peek() == '>':
                    self.addToken(TokenType.IMPLIES)
                    self.consume()
                else:
                    self.addToken(TokenType.EQUAL)
            elif c == '>':
                if self.peek() == '=':
                    self.addToken(TokenType.GEQ)
                    self.consume()
                elif self.peek() == '(':
                    self.addToken(TokenType.LGROUP)
                    self.consume()
                else:
                    self.addToken(TokenType.GT)
            elif c == '<':
                if self.peek() == '=':
                    self.consume()
                    if self.peek() == '>':
                        self.addToken(TokenType.EQUIV)
                        self.consume()
                    else:
                        raise Exception("Unexpected character, expected '>' after '<=', got " + (" nothing" if self.isAtEnd() else self.peek()))
                else:
                    self.addToken(TokenType.LT)
            elif c.isdigit() or c == '.' and self.peek().isdigit():
                self.number()
            elif c.isalpha() or c == '_':
                if c.isupper():
                    self.boolean()
                else:
                    self.identifier()
            else:
                raise Exception("Unexpected character '" + c + "'is not part of Smint grammar")



