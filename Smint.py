#!/bin/python
import sys
from Lexer import Lexer
from Parser import parse
from Interpreter import Context, runProgram

def main():
    if len(sys.argv) >= 2:
        for f in sys.argv[1:]:
            with open(f) as h:
                l = Lexer(h.read())
                l.tokenize()
                runProgram(parse(l.tokens))
    else:
        context = Context()
        s = ""
        while s.strip() != "exit();":
            try:
                s = input("::> ")
            except EOFError:
                exit()
            try:
                if s.strip() == "exit();":
                    continue
                l = Lexer(s.strip())
                l.tokenize()
                runProgram(parse(l.tokens), context=context)
            except Exception as e:
                print(e)


if __name__ == "__main__":
    main()
