-- Terminals : {Identifier : I, 'True', 'False', Number : N, '+', '-', '*', '/', '%', '(', ')', ',' , ':=', '=', '==', '/=', '-|', '=>', '-[', ']-', '>(', ')<', '>=', '=<', '<', '>', '<=>', ';', '!', '&&', '||', '->', '"'}


CHR-> Any character

--String
SR -> '"' CHR + '"'

-- Identifier
SL -> ('a'|...|'z'|'_')
BL -> ('A'|...|'Z'|'_')
DG -> ('0'|...|'9')
I  -> ST (SL|BG|DG)*
N  -> DG+ | DG*  '.' DG+

SB -> I | FC

-- Arithmetic
A  -> P (('+'|'-') P)*
-- Resolution of concatenation or addition is done at runtime
P  -> NG (('*'|'/'|'%') NG)* | SR
NG -> '-'? E
E  -> N | '(' A ')' | SB

-- Boolean
D  -> C ('||' C)*
C  -> U ('&&' U)*
U  -> '!'? CP
CP -> B | A Oa A | V Oc V
B  -> '(' D ')' | 'True' | 'False' | SB 

-- Operators
Oa -> '<' | '>' | '=<' | '>='
Oc -> '==' | '/=' 

-- Value group
V  -> A | D | SG

-- Declaration
L  -> I (':=' V)? ';'

-- Assignment
S  -> I '=' V ';'

-- Condition
CN -> D '=>' ST ('-|' ST)*

-- Loop
LP -> '-[' D ']-' ST

-- Function
FS -> I '(' (I (',' I)*)? ')'
FC -> I '(' (V (',' V)*)? ')'
FD -> FS '<=>' ST

-- Statements
PR -> ST+
ST -> L | S | CN | LP | FD | FC ';' | '>(' PR ')<'

--Grammar alterations for parsing
L' -> (':=' V)? ';'
S' -> '=' V ';'

FN'-> '(' (I (',' I)*)? ')'
FC'-> FN'
FD'-> FN' '<=>' ST

IG -> L' | S' | FC' ';' | FD'
ST'-> I IG | '>(' PR ')<' | LP | '->' V ';'
